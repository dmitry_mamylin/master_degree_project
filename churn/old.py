def encode_raw_categories(raw_data):
    """
    The function doesn't modify the input.
    Input:
        raw_data - pandas.DataFrame with column named "category" of strings.
    Output:
        1. raw_data_new - pandas.DataFrame with column named "category" of integers
           (all other columns are the same as in raw_data).
        2. str_to_id - dict which maps all distinct strings of raw_data.category
           into integers.
        3. id_to_str - a dictionary which represents the inverse transformation of
           str_to_id.
    """
    assert "category" in raw_data.columns, "The data frame must contain the column named 'category'."

    all_categories = sorted([*set(raw_data.category)])
    str_to_id = {str: id for id, str in enumerate(all_categories)}
    id_to_str = {id: str for id, str in str_to_id.items()}

    raw_data_new = raw_data.copy()
    raw_data_new.category = raw_data_new.category.apply(lambda category: str_to_id[category])

    return raw_data_new, str_to_id, id_to_str
