def read_raw_data(filepath):
    import pandas as pd

    raw_data = pd.read_excel(filepath)
    raw_data.columns = ["client_id", "date", "time", "mcc", "target", "amount", 'category']
    return raw_data
